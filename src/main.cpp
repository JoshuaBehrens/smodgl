#include <iostream>
#include "depHelper/stb_image.hpp"
#include "base/Image.hpp"
#include "base/ModifierQueue.hpp"

int main(int argc, char** args)
{
	if (argc > 1)
	{
		std::cout << "file given" << std::endl;

		smodgl::dep::stb::image myImage(smodgl::dep::stb::image::loadFromFile(args[1]));
		if ((bool)myImage)
		{
			smodgl::base::image img(myImage);

			std::cout << "myImage is valid (" << img.getWidth() << "x" << img.getHeight() << ")" << std::endl;

			smodgl::base::modifierQueue queue;
			queue.add(new smodgl::base::modifier_pixels(
				[](smodgl::base::pixel& p, std::size_t x, std::size_t y)
				{
					p.red = 0xFF - p.red;
					p.green = 0xFF - p.green;
					p.blue = 0xFF - p.blue;

					return true;
				})
			);
			queue.add(new smodgl::base::modifier_pixels(
				[](smodgl::base::pixel& p, std::size_t x, std::size_t y)
				{
					uint16_t sum = p.red + p.green + p.blue;
					uint8_t avg = sum / 3;

					p.red = p.green = p.blue = avg;

					return true;
				})
			);

			if (queue.process(img))
			{
				std::cout << "Processed the image in " << queue.count() << " steps" << std::endl;

				img.saveToFile("test.png", smodgl::dep::stb::OutputFileTypes::PNG);
				img.saveToFile("test.tga", smodgl::dep::stb::OutputFileTypes::TGA);
				img.saveToFile("test.bmp", smodgl::dep::stb::OutputFileTypes::BMP);
			}
			else
			{
				std::cout << "Could not process the image" << std::endl;
			}
		}
		else
		{
			std::cout << "myImage is not valid" << std::endl;
		}
	}
	else
	{
		std::cout << "no file given" << std::endl;
	}

	return 0;
}