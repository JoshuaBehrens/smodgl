#ifndef SMODGL_CLI_MODIFIERQUEUE_HPP
#define SMODGL_CLI_MODIFIERQUEUE_HPP

#include <deque>
#include <memory>

#include "Modifier.hpp"
#include "Image.hpp"

namespace smodgl { namespace base {
	class modifierQueue
	{
	private:
		// TODO make it copyable by using shared pointers
		std::deque<modifier*> steps;
	public:
		modifierQueue() = default;
		modifierQueue(const modifierQueue& copy) = delete;
		modifierQueue(modifierQueue&& move);
		~modifierQueue();

		modifierQueue& operator = (const modifierQueue& copy) = delete;
		modifierQueue& operator = (modifierQueue&& move);

		void add(modifier* step);
		std::size_t count() const;

		bool process(image& image);
	};
}}

#endif //SMODGL_CLI_MODIFIERQUEUE_HPP
