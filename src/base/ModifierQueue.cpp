#include <algorithm>

#include "ModifierQueue.hpp"


smodgl::base::modifierQueue::modifierQueue(smodgl::base::modifierQueue &&move)
{
	this->steps = move.steps;
	move.steps.clear();
}

smodgl::base::modifierQueue::~modifierQueue()
{
	std::remove_if(this->steps.begin(), this->steps.end(),
				   [](modifier* item)
				   {
					   delete item;
					   return true;
				   }
	);
}

smodgl::base::modifierQueue& smodgl::base::modifierQueue::operator=(smodgl::base::modifierQueue &&move)
{
	this->steps = move.steps;
	move.steps.clear();

	return *this;
}

void smodgl::base::modifierQueue::add(modifier* step)
{
	this->steps.push_back(step);
}

std::size_t smodgl::base::modifierQueue::count() const
{
	return this->steps.size();
}

bool smodgl::base::modifierQueue::process(smodgl::base::image &image)
{
	for (auto&& step : this->steps)
	{
		if (!step->process(image))
		{
			return false;
		}
	}

	return true;
}
