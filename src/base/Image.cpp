#include <cassert>
#include <algorithm>
#include "Image.hpp"

smodgl::base::image::image(smodgl::dep::stb::image& stbImage)
{
	this->width = stbImage.getWidth();
	this->height = stbImage.getHeight();
	this->data.assign(this->width * this->height, smodgl::base::pixel());

	for (int x = 0; x < stbImage.getWidth(); ++x)
	{
		for (int y = 0; y < stbImage.getHeight(); ++y)
		{
			smodgl::base::pixel& cur = this->at(x, y);
			std::size_t pos = (y * stbImage.getWidth() + x) * 4;

			cur.red = stbImage[pos];
			cur.green = stbImage[pos + 1];
			cur.blue = stbImage[pos + 2];
			cur.alpha = stbImage[pos + 3];
		}
	}
}

smodgl::base::pixel smodgl::base::image::at(std::size_t x, std::size_t y) const
{
	assert(this->positionInImage(x, y));

	return this->data.at(x + y * this->width);
}

smodgl::base::pixel& smodgl::base::image::at(std::size_t x, std::size_t y)
{
	assert(this->positionInImage(x, y));

	return this->data.at(x + y * this->width);
}

bool smodgl::base::image::positionInImage(std::size_t x, std::size_t y) const
{
	return x < this->width && y < this->height;
}

smodgl::base::pixel smodgl::base::image::operator[](std::pair<std::size_t, std::size_t> position) const
{
	return this->at(std::get<0>(position), std::get<1>(position));
}

smodgl::base::pixel& smodgl::base::image::operator[](std::pair<std::size_t, std::size_t> position)
{
	return this->at(std::get<0>(position), std::get<1>(position));
}

int smodgl::base::image::getWidth() const
{
	return this->width;
}

int smodgl::base::image::getHeight() const
{
	return this->height;
}

void smodgl::base::image::saveToFile(std::string filename, smodgl::dep::stb::OutputFileTypes outType) const
{
	std::vector<unsigned char> dataToPass;
	std::for_each(data.begin(), data.end(), [&dataToPass](smodgl::base::pixel p)
	{
		dataToPass.push_back(p.red);
		dataToPass.push_back(p.green);
		dataToPass.push_back(p.blue);
		dataToPass.push_back(p.alpha);
	});

	smodgl::dep::stb::saveToFile(dataToPass, width, height, filename, outType);
}
