#ifndef SMODGL_CLI_MODIFIER_HPP
#define SMODGL_CLI_MODIFIER_HPP

#include <functional>

#include "Image.hpp"

namespace smodgl { namespace base {
	class modifier
	{
	public:
		virtual bool process(image& img) { return false; }
		virtual ~modifier() { }
	};

	class modifier_pixels : public modifier
	{
	private:
		std::function<bool(pixel&, std::size_t, std::size_t)> cb;
	public:
		modifier_pixels(std::function<bool(pixel&, std::size_t, std::size_t)> callback);

		virtual bool process(image& img);
		virtual ~modifier_pixels() { }
	};

	class modifier_image : public modifier
	{
	private:
		std::function<bool(image&)> cb;
	public:
		modifier_image(std::function<bool(image&)> callback);

		virtual bool process(image& img);
		virtual ~modifier_image() { }
	};
}}

#endif //SMODGL_CLI_MODIFIER_HPP
