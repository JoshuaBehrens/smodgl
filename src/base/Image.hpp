#ifndef SMODGL_CLI_IMAGE_HPP
#define SMODGL_CLI_IMAGE_HPP

#include <vector>
#include <tuple>
#include "../depHelper/stb_image.hpp"

namespace smodgl { namespace base {
	struct pixel
	{
		unsigned char red, green, blue, alpha;
	};

	class image
	{
	private:
		std::vector<pixel> data;
		std::size_t width = 0, height = 0;
	public:
		image() = default;
		image(const image& copy) = default;
		image(image&& move) = default;
		image(smodgl::dep::stb::image& stbImage);
		~image() = default;

		image& operator = (const image& copy) = default;
		image& operator = (image&& move) = default;

		pixel at(std::size_t x, std::size_t y) const;
		pixel& at(std::size_t x, std::size_t y);
		bool positionInImage(std::size_t x, std::size_t y) const;

		pixel operator[](std::pair<std::size_t, std::size_t> position) const;
		pixel& operator[](std::pair<std::size_t, std::size_t> position);

		int getWidth() const;
		int getHeight() const;

		void saveToFile(std::string filename, smodgl::dep::stb::OutputFileTypes outType) const;
	};
}}

#endif //SMODGL_CLI_IMAGE_HPP
