#include <cassert>

#include "Modifier.hpp"

smodgl::base::modifier_image::modifier_image(std::function<bool(smodgl::base::image&)> callback) :
	smodgl::base::modifier()
{
	// TODO replace assert with exception
	assert((bool)callback);
	this->cb = callback;
}

bool smodgl::base::modifier_image::process(smodgl::base::image &img)
{
	return this->cb(img);
}

smodgl::base::modifier_pixels::modifier_pixels(std::function<bool(smodgl::base::pixel&, std::size_t, std::size_t)> callback) :
	smodgl::base::modifier()
{
	// TODO replace assert with exception
	assert((bool)callback);
	this->cb = callback;
}

bool smodgl::base::modifier_pixels::process(smodgl::base::image &img)
{
	for (std::size_t y = 0; y < img.getHeight(); ++y)
	{
		for (std::size_t x = 0; x < img.getWidth(); ++x)
		{
			if (!this->cb(img.at(x, y), x, y))
			{
				return false;
			}
		}
	}

	return true;
}