#include <cassert>

#include "stb_image.h"
#include "stb_image_write.h"
#include "stb_image.hpp"

smodgl::dep::stb::image::image(smodgl::dep::stb::image&& move)
{
	this->width = move.width;
	this->height = move.height;
	this->data = move.data;

	move.width = -1;
	move.height = -1;
	move.data = nullptr;
}

smodgl::dep::stb::image::~image()
{
	if (this->data != nullptr)
	{
		stbi_image_free(this->data);
		this->data = nullptr;
	}

	this->width = -1;
	this->height = -1;
}

smodgl::dep::stb::image& smodgl::dep::stb::image::operator = (smodgl::dep::stb::image&& move)
{
	this->width = move.width;
	this->height = move.height;
	this->data = move.data;

	move.width = -1;
	move.height = -1;
	move.data = nullptr;

	return *this;
}

unsigned char smodgl::dep::stb::image::operator[](std::size_t index) const
{
	// TODO make assert an exception
	assert((bool)*this);
	// TODO check index for being in a valid range
	return this->data[index];
}

unsigned char& smodgl::dep::stb::image::operator[](std::size_t index)
{
	// TODO make assert an exception
	assert((bool)*this);
	// TODO check index for being in a valid range
	return this->data[index];
}

smodgl::dep::stb::image::operator bool () const
{
	return this->data != nullptr && this->width > -1 && this->height > -1;
}

int smodgl::dep::stb::image::getWidth() const
{
	return this->width;
}

int smodgl::dep::stb::image::getHeight() const
{
	return this->height;
}

smodgl::dep::stb::image smodgl::dep::stb::image::loadFromFile(std::string filename)
{
	smodgl::dep::stb::image result;

	int comp;
	result.data = stbi_load(filename.c_str(), &result.width, &result.height, &comp, STBI_default);

	return result;
}

void smodgl::dep::stb::saveToFile(std::vector<unsigned char> data, int width, int height, std::string filename, smodgl::dep::stb::OutputFileTypes outputType)
{
	// TODO make assert an exception
	// TODO check that assumption
	assert(data.size() == width * height * 4);

	// 1=Y, 2=YA, 3=RGB, 4=RGBA
	switch (outputType)
	{
		case smodgl::dep::stb::OutputFileTypes::BMP:
			// TODO make assert an exception
			assert(stbi_write_bmp(filename.c_str(), width, height, 4, data.data()));
			break;
		case smodgl::dep::stb::OutputFileTypes::PNG:
			// TODO make assert an exception
			assert(stbi_write_png(filename.c_str(), width, height, 4, data.data(), 4 * width));
			break;
		case smodgl::dep::stb::OutputFileTypes::TGA:
			// TODO make assert an exception
			assert(stbi_write_tga(filename.c_str(), width, height, 4, data.data()));
			break;
	}
}
