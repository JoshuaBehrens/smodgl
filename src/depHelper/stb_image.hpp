#ifndef SMODGL_CLI_STB_IMAGE_H
#define SMODGL_CLI_STB_IMAGE_H

#include <vector>
#include <string>

namespace smodgl { namespace dep { namespace stb {
	enum class OutputFileTypes
	{
		BMP,
		TGA,
		PNG
	};

	class image
	{
	private:
		unsigned char* data = nullptr;
		int width = -1, height = -1;

		image() = default;
	public:
		image(image&& move);
		image(const image&) = delete;
		~image();

		image& operator = (image&& move);
		image& operator = (const image&) = delete;

		unsigned char operator[](std::size_t index) const;
		unsigned char& operator[](std::size_t index);
		operator bool () const;

		int getWidth() const;
		int getHeight() const;

		static image loadFromFile(std::string filename);
	};

	void saveToFile(std::vector<unsigned char> data, int width, int height, std::string filename, OutputFileTypes outputType);
} } }
#endif //SMODGL_CLI_STB_IMAGE_H
