#smodgl
#####super modifiable graphics library

###Summary
This library is meant to fulfill any needs of modifying images with the console. The major feature is the fact that you can easily script any processing steps. So there is no need of any compiled extension. Just write it yourself in seconds with the speed of a compiled application.

###Why do I call it modifiable although it is extensible?
You could guess the abbreviation. Maybe a bit inappropriate.

###Thanks to
* livecoding.tv Community - for being helpful and entertaining
* http://livecoding.tv/alphaglosined/ - for starting an own custom graphics library and giving the idea to roll out my own
* Sean T. Barrett - for his amazing public domain libraries